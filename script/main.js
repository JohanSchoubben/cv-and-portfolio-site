// $('.carousel').carousel({
//     interval: 500
// });
// var navbar = document.getElementById("navbar");
// var stickyNavbar = navbar.offsetTop;
// function getStickyNavbar() {
//     console.log(`Window pageYOffset: ${window.pageYOffset}, navbaroffsettop: ${navbar.offsetTop}`);
//     if (window.pageYOffset >= stickyNavbar) {
//         console.log("Added stickybar");
//         navbar.classList.add("stickyNavbar")
//     } else {
//         console.log("Removed stickybar");
//         navbar.classList.remove("stickyNavbar");
//     }
// }
// window.onscroll = function () { // sets function as value, but doesn't call the function
//     getStickyNavbar()
// };
// window.onscroll = getStickyNavbar();
// $("body").css({ 'padding-top': $('nav.navbar').height() });
// fetch("https://api.apispreadsheets.com/data/10036/", {
// 	method: "POST",
// 	body: JSON.stringify({"data": {"first-name":[""],"last-name":[""],"job-title":[""],"company":[""],"message":[""],"email":[""]}}),
// }).then(res =>{
// 	if (res.status === 201){
// 		console.log('succesfully submitted (main.js)');
// 	}
// 	else{
// 		console.log('there was an error submitting data(main.js)');
// 	}
// })

function displayGreetingText() {
    let greeting = document.getElementById("greeting");
    let now = new Date();
    let hour = now.getHours();
    if (hour < 6) {
        greeting.innerHTML = "Welkom";
    } else if (hour < 11) {
        greeting.innerHTML = "Goedemorgen";
    } else if (hour < 17) {
        greeting.innerHTML = "Goedemiddag";
    } else if (hour < 24) {
        greeting.innerHTML = "Goedeavond";
    }
}
displayGreetingText();

function SubForm() {
    $.ajax({
        url: "https://api.apispreadsheets.com/data/10036/",
        type: "post",
        data: $("#myForm").serializeArray(),
        success: function () {
            alert("Your data are successfully submitted");
            console.log('Form Data Submitted');
        },
        error: function () {
            alert("There was an error submitting your data");
            console.log('There was an error submitting form')
        }
    });
}
